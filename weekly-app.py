import json
import sys
import random
import requests
import os.path
from os import path
import yaml
import re
import matplotlib.pyplot as plt
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError


url_list=[]
with open("./url-list.yaml", "r") as f:
    urls = yaml.load_all(f, Loader=yaml.FullLoader)
    for url in urls:
        for k, v in url.items():
             str1 = ''.join(str(e) for e in v)
             url_list.append(str1)

x = []
y = []
f_list = []
for i in url_list:
    response = requests.get(i)
    op_data = json.loads(response.text)
    mf_name = op_data["meta"]["scheme_name"]
    #f_name = mf_name.split()[0]+"_"+mf_name.split()[1]
    f_name = mf_name.split()[0]
    print (f_name)
    f_list.append('./site/images/{}.png'.format(f_name))
    weekly_data = op_data["data"][:5]
    for i in weekly_data:
        k = list(i.values())
        x.append("{}".format(k[0]))
        y_val = ("{}".format(k[1]))
        y_val = float (y_val)
        y.append(y_val)
    plt.plot(x,y,color='red', marker='o')
    plt.title('Nav of {}'.format(mf_name))
    plt.xlabel('date')
    plt.ylabel('nav')
    plt.grid(True)
    if path.exists('./site/images/{}.png'.format(f_name)):
      print ('./site/images/{}.png exists'.format(f_name))
      print ("updating file name")
      f_name = mf_name.split()[0]+"_"+mf_name.split()[1]
      print ("created file with name : ".format(f_name))
      plt.savefig('./site/images/{}.png'.format(f_name))
      plt.show()
      plt.close()
      x = []
      y = []
    else:
      plt.savefig('./site/images/{}.png'.format(f_name))
      plt.show()
      plt.close()
      x = []
      y = []
