#!/usr/bin/env python3
import requests
import yaml

def check_url (url):
    response = requests.get(url)
    if response.status_code == 200:
       STATUS = 'OK' 
       print (STATUS)
    else :
        STATUS = 'FAILED'
        print (STATUS)
    return(STATUS)


with open("url-list.yaml", "r") as f:
    docs = yaml.load_all(f, Loader=yaml.FullLoader)

    for doc in docs:
        for k, v in doc.items():
             str1 = ''.join(str(e) for e in v)
             print (k)
             check_url (str1)