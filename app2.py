#!/usr/bin/env python3
import json
import sys
import random
import requests
import os
import yaml
import re

from datetime import date

SLACK_WEBHOOK = os.environ["WEBHOOK_URL"]

def check_url (url):
    response = requests.get(url)
    if response.status_code == 200:
       STATUS = 'OK' 
    else :
        STATUS = 'FAILED'
    return(STATUS)

def sch_name (mf_url):
    response = requests.get(mf_url)
    op_data = json.loads(response.text)
    sch_name = (op_data["meta"]["scheme_name"])
    return (sch_name)

def get_val (mf_url):
    response = requests.get(mf_url)
    op_data = json.loads(response.text)
    t_val = (op_data["data"][0])
    y_val = (op_data["data"][1])
    diff = (float(t_val["nav"])) - (float(y_val["nav"]))
    formated_diff = ("{d:1.4f}".format(d=diff))
    sch_name = (op_data["meta"]["scheme_name"])
    return json.dumps(
        { 'Today_Value': t_val, 'Yestreday_Value': y_val, 'Difference_Value': formated_diff})

def slack_notification (message,title):
    url = SLACK_WEBHOOK
    title = (title)
    slack_data = {
        "username": "NotificationBot",
        "icon_emoji": ":metal:",
        "channel" : "#alerts",
        "attachments": [
            {
                "color": "#9733EE",
                "fields": [
                    {
                        "title": title,
                        "value": message,
                        "short": "false",
                    }
                ]
            }
        ]
    }
    byte_length = str(sys.getsizeof(slack_data))
    headers = {'Content-Type': "application/json", 'Content-Length': byte_length}
    response = requests.post(url, data=json.dumps(slack_data), headers=headers)
    if response.status_code != 200:
       raise Exception(response.status_code, response.text)

def graph_gen (i):
    x = []
    y = []
    response = requests.get(i)
    op_data = json.loads(response.text)
    mf_name = op_data["meta"]["scheme_name"]
    f_name = mf_name.split()[0]
    f_list.append('./images/{}.png'.format(f_name))
    weekly_data = op_data["data"][:3]
    for i in weekly_data:
        k = list(i.values())
        x.append("{}".format(k[0]))
        y_val = ("{}".format(k[1]))
        y_val = float (y_val)
        y.append(y_val)
    plt.plot(x,y,color='red', marker='o')
    plt.title('Nav values for 3 days {}'.format(mf_name))
    plt.xlabel('date')
    plt.ylabel('nav')
    plt.grid(True)
    plt.savefig('./images/{}.png'.format(f_name))
    plt.show()
    plt.close()
    x = []
    y = []


url_list=[]
with open("url-list.yaml", "r") as f:
    urls = yaml.load_all(f, Loader=yaml.FullLoader)
    for url in urls:
        for k, v in url.items():
             str1 = ''.join(str(e) for e in v)
             url_list.append(str1)

for i in url_list:
    status = check_url (i)
    message = get_val (i)
    fund_name = sch_name (i)
    title = ("NAV of fund : " + str(fund_name))
    slack_notification (message,title)