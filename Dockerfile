FROM python:3.8
COPY req.txt /tmp/req.txt
RUN pip install -r /tmp/req.txt  && mkdir /app
ENV WEBHOOK_URL=""
WORKDIR /app
COPY app2.py /app/app.py
COPY url-list.yaml /app/url-list.yaml
ENTRYPOINT [ "python" ]
CMD ["/app/app.py"]